package seminarka21lo;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

public class Zivocich{
    private String jmenoZ;
    private int vyskaZ,sirkaZ,koncetinyZ,id;
    private float vahaZ;
    private CheckBox checkbox;
    private Button btn_update;

    Zivocich(int id,String jmeno, int vyska, float vaha, int sirka, int koncetiny){
        this.id = id;
        this.jmenoZ = jmeno;
        this.vyskaZ = vyska;
        this.vahaZ = vaha;
        this.sirkaZ = sirka;
        this.koncetinyZ = koncetiny;
        this.checkbox = new CheckBox();
    }
    public String toString() {
        return jmenoZ + " " + vyskaZ + " " + vahaZ + " " + sirkaZ + " " + koncetinyZ+"\n";
    }
    // GETTERY
    public String getJmenoZ()                  { return jmenoZ;               }    
    public int getKoncetinyZ()                 { return koncetinyZ;           }    
    public int getSirkaZ()                     { return sirkaZ;               }    
    public float getVahaZ()                    { return vahaZ;                }
    public int getVyskaZ()                     { return vyskaZ;               }
    public CheckBox getCheckbox()              { return checkbox;             }
    public Button getBtn()                     { return btn_update;           }
    public int getId()                         { return id;                   }
    
    // SETTER
    public void setJmenoZ(String jmeno)        { this.jmenoZ = jmeno;         }    
    public void setSirkaZ(int sirka)           { this.sirkaZ = sirka;         }    
    public void setKoncetinyZ(int koncetiny)   { this.koncetinyZ = koncetiny; }
    public void setVahaZ(float vaha)           { this.vahaZ = vaha;           }
    public void setVyskaZ(int vyska)           { this.vyskaZ = vyska;         }
    public void setCheckbox(CheckBox checkbox) { this.checkbox=checkbox;      }
    public void setBtn(Button btn_update)      { this.btn_update=btn_update;  }
    public void setId(int ID)                  { this.id = ID;                }
}// CLASS END
