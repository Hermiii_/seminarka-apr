package seminarka21lo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController implements Initializable {
    @FXML Button btn_login;
    @FXML TextField txt_name;
    @FXML PasswordField txt_pwd;
    @FXML Label chyba;
    public String udaje = "";
    private Scene secondScene;
    public static int poziceP,rowP;

    public void setSecondScene(Scene scene) { secondScene = scene; }
    
    public void loginBtnClick(ActionEvent event) throws IOException{
        ArrayList<String> data = new ArrayList<>();
        try (BufferedReader r = new BufferedReader(new FileReader("src\\seminarka21lo\\data\\employees.txt"))) {
            while(r.ready()) {
                data.add(r.readLine());
            }
            r.close();
        }catch (IOException e) {
            e.printStackTrace();
        }

        String [][] arr = new String[data.size()][];
        for (int i = 0; i<data.size(); i++) {
            arr[i] = data.get(i).split(";");
        }
        
        chyba.setText("");
        
        for (int row = 0; row < arr.length; row++) {
            if (txt_name.getText().equals(arr[row][0]) && txt_pwd.getText().equals(arr[row][3])) {
                chyba.setText("");
                txt_name.setText("");
                txt_pwd.setText("");
                
                poziceP = Integer.parseInt(arr[row][2]);
                rowP = row;
                                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("Dashboard.fxml"));
                Parent root = loader.load();
                //DashboardController controller = loader.getController();
                
                Scene secondScene = new Scene(root);
                Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
                primaryStage.setScene(secondScene);
            }
            else {
                chyba.setText("Chybné údaje!");
            }

        }
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("start login");
        // vypnutí tlačítka
        BooleanBinding bb = new BooleanBinding() {
            { super.bind(txt_name.textProperty(),txt_pwd.textProperty()); }

            @Override
            protected boolean computeValue() {
                return (txt_name.getText().isEmpty() || txt_pwd.getText().isEmpty());
            }
        };
        btn_login.disableProperty().bind(bb);       
        
    }    
}
