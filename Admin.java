package seminarka21lo;

public class Admin extends User{
    private String telefonE;
    
    Admin(int id, String jmeno, int vek, String pozice, String heslo, String telefon) {
        super(id, jmeno, vek, pozice, heslo);
        this.telefonE = telefon;
    }
    
    public String toString() {
        return getIdE() + " " + getJmenoE() + " " + getVekE() + " " + getPoziceE() + " " + getHesloE() + " " + telefonE+ " " + "\n";
    }
    
    public final String getTelefonE()             { return telefonE;         }
    public final void setTelefonE(String telefon) { this.telefonE = telefon; }
}
