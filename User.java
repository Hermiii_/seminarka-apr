
package seminarka21lo;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

public class User {
    private String jmenoE, poziceE, hesloE;
    private int vekE,idE;
    private CheckBox checkboxE;
    private Button btn_updateE;
    
    User(int idE, String jmeno, int vek, String pozice, String heslo) {
        this.idE = idE;
        this.jmenoE = jmeno;
        this.vekE = vek;
        this.poziceE = pozice;
        this.hesloE = heslo;
        this.checkboxE = new CheckBox();
    }
    public String toString() {
        return  idE + " " + jmenoE + " " + vekE + " " + poziceE + " " + hesloE + "\n";
    }
    //GETTERS
    public final String getJmenoE()             { return jmenoE;      }    
    public final String getPoziceE()            { return poziceE;     }    
    public final int getVekE()                  { return vekE;        }
    public final String getHesloE()             { return hesloE;      }
    public int getIdE()                         { return idE;         }
    public CheckBox getCheckboxE()              { return checkboxE;   }
    public Button getBtn()                      { return btn_updateE; }
    
    //SETTERS
    public final void setJmenoE(String jmeno)   { this.jmenoE = jmeno;           }    
    public final void setVekE(int vek)          { this.vekE = vek;               }        
    public final void setPoziceE(String pozice) { this.poziceE = pozice;         }    
    public final void setHesloE(String heslo)   { this.hesloE = heslo;           }
    public void setIdE(int ID)                  { this.idE = ID;                 }
    public void setCheckboxE(CheckBox checkbox) { this.checkboxE = checkbox;     }
    public void setBtn(Button btn_update)       { this.btn_updateE = btn_update; }
    
} // CLASS END
