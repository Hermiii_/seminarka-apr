package seminarka21lo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.util.Callback;

import static seminarka21lo.LoginController.poziceP;
import static seminarka21lo.LoginController.rowP;

public class DashboardController implements Initializable {

    @FXML Label lbl_clock;
    @FXML Button btn_out,btn_home, btn_an, btn_emp,btn_delA,btn_addA,btn_updateA,btn_updateE,btn_addE,btn_delE;
    @FXML Pane pn_home, pn_an, pn_emp;
    @FXML TableView<Zivocich> tableAn;
    @FXML TableColumn<Zivocich, String> clmn_nameA;
    @FXML TableColumn<Zivocich, Integer> clmn_nohy;
    @FXML TableColumn<Zivocich, Float> clmn_weight;
    @FXML TableColumn<Zivocich, Integer> clmn_width;
    @FXML TableColumn<Zivocich, Integer> clmn_height;
    @FXML TableColumn<Zivocich, String> clmn_del;
    @FXML TableView<Object> tableE;
    @FXML TableColumn<User, String> clmn_nameE;
    @FXML TableColumn<User, Integer> clmn_vekE;
    @FXML TableColumn<User, Integer> clmn_poziceE;
    @FXML TableColumn<User, Integer> clmn_hesloE;
    @FXML TableColumn<Admin, String> clmn_telefonE;
    @FXML TableColumn<User, String> clmn_delE;
    @FXML TextField txt_nameA,txt_vahaA,txt_widthA,txt_heightA,txt_nameE,txt_hesloE,txt_telefonE;
    @FXML RadioButton rb_chovatel, rb_admin;
    @FXML ToggleGroup rbGroup;
    ArrayList<String> dataAnimals = new ArrayList<>();
    List<Zivocich> Zver = new ArrayList<Zivocich>();
    ArrayList<String> dataEmp = new ArrayList<>();
    List<Object> Zamestnanci = new ArrayList<Object>();
    @FXML ComboBox<String> cb_nohy;
    @FXML ComboBox<String> cb_vekE;
    int rowNo,rowNo2;
    String loggedName,loggedPos,loggedPass;
    boolean isLogged = false;
    static Thread timerThread;
    private Scene firstScene;
    int poziceLogged;

    public void setFirstScene(Scene scene) { firstScene = scene; }
    public void logoutClickAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent firstPane = loginLoader.load();
        Scene firstScene = new Scene(firstPane);
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(firstScene);
    }
    
    /**
     * Pomocí threadu dostávám čas, který je použit pouze pro dekorativní účely
     * na main Pane.
     */
    void hodiny() {
        timerThread = new Thread(() -> {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss, dd/MM/yyyy");
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                final String time = simpleDateFormat.format(new Date());
                Platform.runLater(() -> {
                    lbl_clock.setText(time);
                });
            }
        });
        timerThread.start();
    }

    public void handleButtonAction(ActionEvent event) {
        if(event.getSource() == btn_home)     { pn_home.toFront(); }
        else if(event.getSource() == btn_emp) { pn_emp.toFront();  }
        else if(event.getSource() == btn_an)  { pn_an.toFront();   }
    }
    
    public void handleRadioButtonAction(ActionEvent event) {
        if(rb_chovatel.isSelected()){
            txt_telefonE.setDisable(true);
        }
        else if(rb_admin.isSelected()){
            txt_telefonE.setDisable(false);
        }
    }
        
    public final int getPoziceU()             { return poziceLogged; }
    public final void setPoziceU(int poziceLogged) { this.poziceLogged = poziceLogged;}

    public void empToTable() {
        setPoziceU(poziceP);
        clmn_nameE.setCellValueFactory(new PropertyValueFactory<>("jmenoE"));
        clmn_vekE.setCellValueFactory(new PropertyValueFactory<>("vekE"));
        clmn_poziceE.setCellValueFactory(new PropertyValueFactory<>("poziceE"));
        clmn_hesloE.setCellValueFactory(new PropertyValueFactory<>("hesloE"));
        
        
        TableColumn<Object, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Object, Void>, TableCell<Object, Void>> cellFactory = new Callback<TableColumn<Object, Void>, TableCell<Object, Void>>() {

            @Override
            public TableCell<Object, Void> call(final TableColumn<Object, Void> param) {
                final TableCell<Object, Void> cell = new TableCell<Object, Void>() {
                    private final Button btnE = new Button("Action");
                    {
                        btnE.setOnAction((ActionEvent event) -> {
                            Object data = getTableView().getItems().get(getIndex());
                            String d = data.toString();
                            String[] p = d.split(" ");
                            if(p.length == 5) {
                                User u = (User)data;
                                rowNo2 = u.getIdE();
                                txt_nameE.setText(u.getJmenoE());
                                cb_vekE.setValue(Integer.toString(u.getVekE()));
                                if(u.getPoziceE().equals("0")){
                                    rb_chovatel.setSelected(true);
                                    txt_telefonE.setText("");
                                    txt_telefonE.setDisable(true);
                                }
                                txt_hesloE.setText(u.getHesloE());
                            }
                            else {
                                Admin a = (Admin)data;
                                rowNo2 = a.getIdE();
                                txt_nameE.setText(a.getJmenoE());
                                cb_vekE.setValue(Integer.toString(a.getVekE()));
                                if(a.getPoziceE().equals("1")){
                                    rb_admin.setSelected(true);
                                    txt_telefonE.setDisable(false);
                                }
                                txt_hesloE.setText(a.getHesloE());
                                txt_telefonE.setText(a.getTelefonE());
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) { setGraphic(null); }
                        else { setGraphic(btnE); }
                    }
                };//buňka
                return cell;
            }//sloupec
        };
        if(getPoziceU() == 1){
            clmn_delE.setCellValueFactory(new PropertyValueFactory<>("checkboxE"));
            colBtn.setCellFactory(cellFactory);
            tableE.getColumns().add(colBtn);
        }
        else {
            clmn_delE.setVisible(false);
            btn_delE.setVisible(false);
            btn_delA.setVisible(false);
            btn_addA.setVisible(false);
            btn_addE.setVisible(false);
            clmn_del.setVisible(false);
            
            rbGroup.getToggles().forEach(toggle -> {
                Node node = (Node) toggle;
                node.setDisable(true);
            });
            
        }
        try (BufferedReader r = new BufferedReader(new FileReader("src\\seminarka21lo\\data\\employees.txt"))) {
            while(r.ready()) { dataEmp.add(r.readLine()); }
            r.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
        
        String [][] arr = new String[dataEmp.size()][5];
        
        for (int i = 0; i<dataEmp.size(); i++) {
            arr[i] = dataEmp.get(i).split(";");
        }
        
        for (int row = 0; row < arr.length; row++) {
            if(arr[row].length == 4){
                Zamestnanci.add(new User(
                        row,arr[row][0],
                        Integer.parseInt(arr[row][1]),
                        arr[row][2],
                        arr[row][3]
                ));
                tableE.getItems().add(new User(
                        row,arr[row][0],
                        Integer.parseInt(arr[row][1]),
                        arr[row][2],
                        arr[row][3]
                ));       
            }
            else {
                clmn_telefonE.setCellValueFactory(new PropertyValueFactory<>("telefonE"));
                Zamestnanci.add(new Admin(
                        row,arr[row][0],
                        Integer.parseInt(arr[row][1]),
                        arr[row][2],
                        arr[row][3],
                        arr[row][4]
                ));
                tableE.getItems().add(new Admin(
                        row,arr[row][0],
                        Integer.parseInt(arr[row][1]),
                        arr[row][2],
                        arr[row][3],
                        arr[row][4]
                ));
            }
        }// for
        
        if(getPoziceU() == 0){
            Object data = Zamestnanci.get(rowP);
            User uz = (User)data;
            txt_nameE.setText(uz.getJmenoE());
            cb_vekE.setValue(Integer.toString(uz.getVekE()));
            txt_hesloE.setText(uz.getHesloE());
        }
    }
    
    public void changeE(ActionEvent event) {
        if(getPoziceU() == 1){ //Zjistím, jestli je přihlášený admin
            Object data = Zamestnanci.get(rowNo2); //získám info o řádku, který chci editovat
            if(rb_chovatel.isSelected()) { //zkontroluju jestli je vybraný chovatel
                User u = (User)data; // pokud je vybraný chovatel přetypuju data na User -> chovatele
                int pom = rowNo2;
                String jmeno,heslo;
                int vek;
                if(u.getPoziceE().equals("1") && rb_chovatel.isSelected()) { //Porovnám zdali teď vybraný chovatel je zapsaný v db jako admin
                    Zamestnanci.set(rowP,new User(rowP,txt_nameE.getText(),Integer.parseInt(cb_vekE.getValue()),"0",txt_hesloE.getText())); // jestliže je, přepíšu jeho pozici na chovatele - 0 a změním jeho třídu z Admin na User
                }
                else { //zde pouze přepíšu data
                    u.setJmenoE(txt_nameE.getText());
                    u.setHesloE(txt_hesloE.getText());
                    u.setVekE(Integer.parseInt(cb_vekE.getValue()));
                    u.setPoziceE("0");
                }
            }
            else { // pokud je vybraný admin dělám stejnou kontrolu jako u chovatele
                User u = (User)data;
                int pom = rowNo2;
                String jmeno,heslo,telefon;
                int vek;
                if(u.getPoziceE().equals("0") && rb_admin.isSelected()) {
                    jmeno = txt_nameE.getText();
                    heslo = txt_hesloE.getText();
                    vek = Integer.parseInt(cb_vekE.getValue());
                    telefon = txt_telefonE.getText();
                    Zamestnanci.set(pom,new Admin(pom,jmeno,vek, "1",heslo,telefon));
                }
                else {
                    Admin a = (Admin)data;
                    a.setJmenoE(txt_nameE.getText());
                    a.setHesloE(txt_hesloE.getText());
                    a.setVekE(Integer.parseInt(cb_vekE.getValue()));
                    a.setPoziceE("1");
                    a.setTelefonE(txt_telefonE.getText());
                }
            }
        } // pokud není přihlášený admin, může zaměstnanec editovat pouze sebe
        else {
            Object data = Zamestnanci.get(rowP);
            User uz = (User)data;
            String jmeno,heslo;
            int vek;
            jmeno = txt_nameE.getText();
            heslo = txt_hesloE.getText();
            vek = Integer.parseInt(cb_vekE.getValue());
            Zamestnanci.set(rowP,new User(rowP,jmeno,vek,"0",heslo));
        }
        refreshTableE();
        wToE();
    }
    
    public void addE(ActionEvent event) {
        if(rb_admin.isSelected()){
            Zamestnanci.add(new Admin(
                tableE.getItems().size(),
                txt_nameE.getText(),
                Integer.parseInt(cb_vekE.getValue()),
                "1",
                txt_hesloE.getText(),
                txt_telefonE.getText()
            ));
        refreshTableE();
        wToE();
        }
        else {
            Zamestnanci.add(new User(
                tableE.getItems().size(),
                txt_nameE.getText(),
                Integer.parseInt(cb_vekE.getValue()),
                "0",
                txt_hesloE.getText()
            ));
            txt_telefonE.setText("");
            refreshTableE();
            wToE();
        }
        txt_nameE.setText("");
        cb_vekE.setValue("18");
        rb_chovatel.setSelected(true);
        txt_hesloE.setText("");
    }

    public void animalsToTable() {
        clmn_nameA.setCellValueFactory(new PropertyValueFactory<>("jmenoZ"));
        clmn_nohy.setCellValueFactory(new PropertyValueFactory<>("koncetinyZ"));
        clmn_weight.setCellValueFactory(new PropertyValueFactory<>("vahaZ"));
        clmn_width.setCellValueFactory(new PropertyValueFactory<>("sirkaZ"));
        clmn_height.setCellValueFactory(new PropertyValueFactory<>("vyskaZ"));
        clmn_del.setCellValueFactory(new PropertyValueFactory<>("checkbox"));

        TableColumn<Zivocich, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Zivocich, Void>, TableCell<Zivocich, Void>> cellFactory = new Callback<TableColumn<Zivocich, Void>, TableCell<Zivocich, Void>>() {

            @Override
            public TableCell<Zivocich, Void> call(final TableColumn<Zivocich, Void> param) {
                final TableCell<Zivocich, Void> cell = new TableCell<Zivocich, Void>() {
                    private final Button btn = new Button("Action");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Zivocich data = getTableView().getItems().get(getIndex());
                            rowNo = data.getId();
                            System.out.println(getTableView().getItems().get(getIndex()));
                            txt_nameA.setText(data.getJmenoZ());
                            txt_heightA.setText(Integer.toString(data.getVyskaZ()));
                            txt_widthA.setText(Integer.toString(data.getSirkaZ()));
                            txt_vahaA.setText(Float.toString(data.getVahaZ()));
                            cb_nohy.setValue(Integer.toString(data.getKoncetinyZ()));
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) { setGraphic(null); }
                        else { setGraphic(btn); }
                    }
                };//buňka
                return cell;
            }//sloupec
        };
        colBtn.setCellFactory(cellFactory);
        tableAn.getColumns().add(colBtn);

        try (BufferedReader r = new BufferedReader(new FileReader("src\\seminarka21lo\\data\\animals.txt"))) {
            while(r.ready()) { dataAnimals.add(r.readLine()); }
            r.close();
        }catch (IOException e) {
            e.printStackTrace();
        }

        String [][] arr = new String[dataAnimals.size()][];
        for (int i = 0; i<dataAnimals.size(); i++) {
            arr[i] = dataAnimals.get(i).split(";");
        }

        for (int row = 0; row < arr.length; row++) {
            Zver.add(new Zivocich(
                    row,arr[row][0],
                    Integer.parseInt(arr[row][1]),
                    Float.parseFloat(arr[row][2]),
                    Integer.parseInt(arr[row][3]),
                    Integer.parseInt(arr[row][4])
            ));
            tableAn.getItems().add(new Zivocich(
                    row,arr[row][0],
                    Integer.parseInt(arr[row][1]),
                    Float.parseFloat(arr[row][2]),
                    Integer.parseInt(arr[row][3]),
                    Integer.parseInt(arr[row][4])
            ));
        }
    }

    void wToAn(){
        try {
            FileWriter myWriter = new FileWriter("src\\seminarka21lo\\data\\animals.txt");
            for(Zivocich v: Zver) {
                myWriter.write(v.getJmenoZ()+";"
                        + v.getVyskaZ()+";"
                        + v.getVahaZ()+";"
                        + v.getSirkaZ()+";"
                        + v.getKoncetinyZ()+"\n"
                );
            }
            myWriter.close();

        }catch(IOException e){
            System.out.println("Chyba");
        }
    }

    void wToE(){
        try {
            FileWriter myWriter = new FileWriter("src\\seminarka21lo\\data\\employees.txt");
            for(Object v: Zamestnanci) {
                String s = v.toString();
                String[] p = s.split(" ");
                if(p.length>5){
                    Admin a = (Admin)v;
                    myWriter.write(a.getJmenoE()+";"
                            + a.getVekE()+";"
                            + a.getPoziceE()+";"
                            + a.getHesloE()+";"
                            + a.getTelefonE()+"\n"
                    );
                }
                else {
                    User u = (User)v;
                    myWriter.write(u.getJmenoE()+";"
                            + u.getVekE()+";"
                            + u.getPoziceE()+";"
                            + u.getHesloE()+"\n"
                    );
                }
            }
            myWriter.close();

        }catch(IOException e){
            System.out.println("Chyba");
        }
    }

    void refreshTableAn() {
        tableAn.getSelectionModel().clearSelection();
        tableAn.getItems().clear();
        tableAn.getItems().addAll(Zver);
    }
    
    void refreshTableE() {
        tableE.getSelectionModel().clearSelection();
        tableE.getItems().clear();
        tableE.getItems().addAll(Zamestnanci);
    }

    public void delA(ActionEvent event) {
        ObservableList<Zivocich> list = FXCollections.observableArrayList();
        for(Zivocich v:Zver){if(v.getCheckbox().isSelected()){list.add(v);}}
        Zver.removeAll(list);
        refreshTableAn();
        wToAn();
    }
    
    public void delE(ActionEvent event) {
       ObservableList<Object> list2 = FXCollections.observableArrayList();         
       for(Object v : Zamestnanci){
           String s = v.toString();
           String[] p = s.split(" ");

           if(p.length < 6) {
               User u = (User)v;
               if(u.getCheckboxE().isSelected()){
                   list2.add(v);
               }
           }
           else if(p.length>5){
               Admin u = (Admin)v;
               if(u.getCheckboxE().isSelected()){
                   list2.add(v);
               }
           }
       }   
       Zamestnanci.removeAll(list2);
       refreshTableE();
       wToE();
    }

    public void addA(ActionEvent event) {
        Zver.add(new Zivocich(
                tableAn.getItems().size(),
                txt_nameA.getText(),
                Integer.parseInt(txt_heightA.getText()),
                Float.parseFloat(txt_vahaA.getText()),
                Integer.parseInt(txt_widthA.getText()),
                Integer.parseInt(cb_nohy.getValue())
        ));
        refreshTableAn();
        wToAn();
        
        txt_nameA.setText("");
        txt_widthA.setText("");
        txt_vahaA.setText("");
        txt_heightA.setText("");
        cb_nohy.setValue("4");
    }
    
    public void changeA(ActionEvent event) {
        Zivocich data = Zver.get(rowNo);
        data.setJmenoZ(txt_nameA.getText());
        data.setVyskaZ(Integer.parseInt(txt_heightA.getText()));
        data.setSirkaZ(Integer.parseInt(txt_widthA.getText()));
        data.setVahaZ(Float.parseFloat(txt_vahaA.getText()));
        data.setKoncetinyZ(Integer.parseInt(cb_nohy.getValue()));
        refreshTableAn();
        wToAn();
        txt_nameA.setText("");
        txt_heightA.setText("");
        txt_widthA.setText("");
        txt_vahaA.setText("");
        cb_nohy.setValue("4");
    }

    public void handleLogoutBtn(ActionEvent event) throws IOException {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Odhlášení z aplikace");
        alert.setHeaderText("Opravdu se chcete odhlásit?");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK){
            txt_nameA.setText("");
            txt_vahaA.setText("");
            txt_widthA.setText("");
            txt_heightA.setText("");
            txt_nameE.setText("");
            txt_hesloE.setText("");
            txt_telefonE.setText("");
            logoutClickAction(event);
            timerThread.stop();
        }
    }
                
    @Override
    public void initialize(URL url, ResourceBundle rb) {       
        System.out.println("start dash");
        txt_telefonE.setDisable(true);
        cb_nohy.getItems().addAll("1", "2", "3", "4","5","6","7","8","88","652","750");
        cb_nohy.setValue("4");
        
        for(int i = 15; i<70; i++){
            cb_vekE.getItems().add(Integer.toString(i));
        }
        cb_vekE.setValue("18");
        hodiny();
        animalsToTable();
        empToTable();
        
        BooleanBinding bb = new BooleanBinding() {
            { super.bind(txt_nameE.textProperty(),txt_hesloE.textProperty()); }

            @Override
            protected boolean computeValue() {
                return (txt_nameE.getText().isEmpty() || txt_hesloE.getText().isEmpty());
            }
        };
        btn_addE.disableProperty().bind(bb);
        btn_updateE.disableProperty().bind(bb);
        
        BooleanBinding bb2 = new BooleanBinding() {
            { super.bind(txt_nameA.textProperty(),txt_widthA.textProperty(),txt_vahaA.textProperty(),txt_heightA.textProperty()); }

            @Override
            protected boolean computeValue() {
                return (txt_nameA.getText().isEmpty() || txt_widthA.getText().isEmpty() || txt_vahaA.getText().isEmpty() || txt_heightA.getText().isEmpty());
            }
        };
        btn_addA.disableProperty().bind(bb2);
        btn_updateA.disableProperty().bind(bb2);
    }
}
